<?php
return array(
	'*' => array(
		'manifestPath' => 'public/assets/rev-manifest.json',
		'assetsBasePath' => '/public/assets/',
		'assetUrlPrefix' => '',
		'shouldUseQueryString' => (getenv('APP_ENV') !== 'local')
	),
);