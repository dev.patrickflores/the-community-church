<?php
namespace Craft;
// use craft\elements\Entry;
// use craft\helpers\UrlHelper;

return [
    'endpoints' => [
        'api/v1/videos/<videoCategoryId:\d+>.json' => function($videoCategoryId) {
            $videoCategory = craft()->categories->getCategoryById($videoCategoryId);
            return [
            'elementType' => ElementType::Entry,
            'criteria' => ['section' => 'videos', 'relatedTo' => [
                'targetElement' => $videoCategory
            ]],
            'transformer' => function(EntryModel $entry) {
                $result = [];
                $result['id'] = $entry->id;
                $result['title'] = $entry->title;
                $result['type'] = $entry->type->handle;
                $result['slug'] = $entry->slug;
                $result['url'] = $entry->url;
                $timestamp = strtotime($entry->postDate);
                $new_date_format = date('M d, Y ', $timestamp);
                $result['postDate'] = $new_date_format;
                $featuredImage = $entry->featuredImage->first();
                if($featuredImage) {
                    $featuredImageUrl = UrlHelper::getSiteUrl('assets/images').'/_videoImage/'. $featuredImage->filename;
                } else {
                    $featuredImageUrl = '/assets/images/img-video-post-default.jpg';
                }
                $result['image'] = $featuredImageUrl;
                // Entry Video Categories
                $categories = [];
                foreach ($entry->videoCategory as $category) {
                    $categoryData = [];
                    $categoryData['title'] = $category->title;
                    $categoryData['id'] = $category->id;
                    $categoryData['slug'] = $category->slug;
                    $categoryData['url'] = $category->url;
                    $categories[] = $categoryData;
                }
                $result['videoCategories'] = $categories;
                $result['isNew'] = $entry->isNew->label;
                $result['popupId'] = $entry->videoPopupId;
                return $result;
            },
            'paginate' => true,
            'elementsPerPage' => 9,
            'cache' => false
            ];
        },
        'api/v1/blog/<blogCategoryId:\d+>.json' => function($blogCategoryId) {
            $blogCategory = craft()->categories->getCategoryById($blogCategoryId);
            return [
            'elementType' => ElementType::Entry,
            'criteria' => ['section' => 'blog', 'relatedTo' => [
                'targetElement' => $blogCategory
            ]],
            'transformer' => function(EntryModel $entry) {
                $result = [];
                $result['id'] = $entry->id;
                $result['title'] = $entry->title;
                $result['type'] = $entry->type->handle;
                $result['slug'] = $entry->slug;
                $result['url'] = $entry->url;
                $result['body'] = (string) substr(strip_tags($entry->body),0,255).'...';
                $timestamp = strtotime($entry->postDate);
                $new_date_format = date('M d, Y ', $timestamp);
                $result['postDate'] = $new_date_format;
                // Featured Image
                $featuredImage = $entry->featuredImage->first();
                if($featuredImage) {
                    $featuredImageUrl = UrlHelper::getSiteUrl('assets/images').'/_blogImage/'. $featuredImage->filename;
                } else {
                    $featuredImageUrl = '/assets/images/img-blog-post-default.jpg';
                }
                $result['image'] = $featuredImageUrl;
                // Entry Blog Categories
                $categories = [];
                foreach ($entry->blogCategory as $category) {
                    $categoryData = [];
                    $categoryData['title'] = $category->title;
                    $categoryData['id'] = $category->id;
                    $categoryData['slug'] = $category->slug;
                    $categoryData['url'] = $category->url;
                    $categories[] = $categoryData;
                }
                $result['blogCategories'] = $categories;
                return $result;
            },
            'paginate' => true,
            'elementsPerPage' => 5,
            'cache' => false
            ];
        },
        'api/v1/videos.json' => [
            'elementType' => ElementType::Entry,
            'criteria' => ['section' => 'videos'],
            'transformer' => function(EntryModel $entry) {
                $result = [];
                $result['id'] = $entry->id;
                $result['title'] = $entry->title;
                $result['type'] = $entry->type->handle;
                $result['slug'] = $entry->slug;
                $result['url'] = $entry->url;
                $timestamp = strtotime($entry->postDate);
                $new_date_format = date('M d, Y ', $timestamp);
                $result['postDate'] = $new_date_format;
                $featuredImage = $entry->featuredImage->first();
                if($featuredImage) {
                    $featuredImageUrl = UrlHelper::getSiteUrl('assets/images').'/_videoImage/'. $featuredImage->filename;
                } else {
                    $featuredImageUrl = '/assets/images/img-video-post-default.jpg';
                }
                $result['image'] = $featuredImageUrl;
                // Entry Video Categories
                $categories = [];
                foreach ($entry->videoCategory as $category) {
                    $categoryData = [];
                    $categoryData['title'] = $category->title;
                    $categoryData['id'] = $category->id;
                    $categoryData['slug'] = $category->slug;
                    $categoryData['url'] = $category->url;
                    $categories[] = $categoryData;
                }
                $result['videoCategories'] = $categories;
                $result['isNew'] = $entry->isNew->label;
                $result['popupId'] = $entry->videoPopupId;
                return $result;
            },
            'paginate' => true,
            'elementsPerPage' => 9,
            'cache' => false
        ],
        'api/v1/blog.json' => [
            'elementType' => ElementType::Entry,
            'criteria' => ['section' => 'blog'],
            'transformer' => function(EntryModel $entry) {
                $result = [];
                $result['id'] = $entry->id;
                $result['title'] = $entry->title;
                $result['type'] = $entry->type->handle;
                $result['slug'] = $entry->slug;
                $result['url'] = $entry->url;
                $result['body'] = (string) substr(strip_tags($entry->body),0,259).'...';
                $timestamp = strtotime($entry->postDate);
                $new_date_format = date('M d, Y ', $timestamp);
                $result['postDate'] = $new_date_format;
                // Featured Image
                $featuredImage = $entry->featuredImage->first();

                if($featuredImage) {
                    $featuredImageUrl = UrlHelper::getSiteUrl('assets/images').'/_blogImage/'. $featuredImage->filename;
                } else {
                    $featuredImageUrl = '/assets/images/img-blog-post-default.jpg';
                }
                $result['image'] = $featuredImageUrl;
                // Entry Blog Categories
                $categories = [];
                foreach ($entry->blogCategory as $category) {
                    $categoryData = [];
                    $categoryData['title'] = $category->title;
                    $categoryData['id'] = $category->id;
                    $categoryData['slug'] = $category->slug;
                    $categoryData['url'] = $category->url;
                    $categories[] = $categoryData;
                }
                $result['blogCategories'] = $categories;
                return $result;
            },
            'paginate' => true,
            'elementsPerPage' => 5,
            'cache' => false
        ],
    ]
];
