# Community Church Craft Project

## Requirements

- [Composer](https://getcomposer.org)
- Python 2.7.x
- [NPM](https://www.npmjs.com)
- [PHP 7.0+](http://php.net)

## Installation (With Docker)
- Install all dependencies
- Clone the project and copy the `.env.example` file to `.env`
- Install php dependencies: `composer install --ignore-platform-reqs`
- Install node/build dependencies: `npm install` (Note: Make sure you're using python 2.7!)
- Install bower dependencies: `bower install`
- Start servers: `docker-compose up`

If there are no errors, you will be able to visit localhost:8080 to see the proxied browser-sync site and localhost:8000 to see the server.

If you have errors on docker-compose up, you probably have another instance running. You can always docker-compose down in the project directory to stop the services.
Another possible cause for error is if you have ports 8000, 8080, or 3306 occupied already.

