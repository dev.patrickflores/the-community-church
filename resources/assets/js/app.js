$(document).foundation();
// transEndEventNames = {
//     'WebkitTransition': 'webkitTransitionEnd',
//     'MozTransition': 'transitionend',
//     'OTransition': 'oTransitionEnd',
//     'msTransition': 'MSTransitionEnd',
//     'transition': 'transitionend'
// },
//     transEndEventName = transEndEventNames[Modernizr.prefixed('transition')],
//     support = { transitions: Modernizr.csstransitions };


(function() {
    let triggerBttn = document.getElementById('trigger-overlay'),
        overlay = document.querySelector('div.overlay'),
        closeBttn = overlay.querySelector('.menu-button');
    let isAnimating;
    let morphSearch = document.getElementById('morphsearch'),
        input = morphSearch.querySelector('input.morphsearch-input'),
        ctrlClose = morphSearch.querySelector('span.morphsearch-close'),
        isOpen = isAnimating = false,
        toggleSearch = function(evt) {
            if (evt.type.toLowerCase() === 'focus' && isOpen) return false;

            var offsets = morphsearch.getBoundingClientRect();
            if (isOpen) {
                classie.remove(morphSearch, 'open');
                if (input.value !== '') {
                    setTimeout(function() {
                        classie.add(morphSearch, 'hideInput');
                        setTimeout(function() {
                            classie.remove(morphSearch, 'hideInput');
                            input.value = '';
                        }, 300);
                    }, 500);
                }

                input.blur();
            } else {
                classie.add(morphSearch, 'open');
                jQuery('html').addClass('y-overflow');
            }
            isOpen = !isOpen;
        };

    function toggleOverlay() {
        if (classie.has(overlay, 'open')) {
            classie.remove(overlay, 'open');
        } else if (!classie.has(overlay, 'close')) {
            classie.add(overlay, 'open');
        }
    }

    triggerBttn.addEventListener('click', toggleOverlay);
    closeBttn.addEventListener('click', toggleOverlay);
    input.addEventListener('focus', toggleSearch);
    ctrlClose.addEventListener('click', toggleSearch);

    document.addEventListener('keydown', function(ev) {
        var keyCode = ev.keyCode || ev.which;
        if (keyCode === 27 && isOpen) {
            toggleSearch(ev);
        }
    });

    $('.morphsearch-close').on('click', function() {
        $('html').removeClass('y-overflow');
    });

    $('.trigger.menu-button').click(function() {
        $(this).addClass('open');
        $('.overlay.overlay-hugeinc .menu-button').addClass('open');
        $('html').addClass('y-overflow');
    });
    $('.overlay.overlay-hugeinc .menu-button').on('click', function() {
        $('.trigger.menu-button').removeClass('open');
        $(this).removeClass('open');
        $('html').removeClass('y-overflow');
    });
    $('body').on('click', 'a[data-rel^=lightcase]', function(e) {
        var href = $(this).attr('href');
        lightcase.start({
            href: href
        });
        e.preventDefault();
    });

    $(document).on('click', '.scroll-link a[href^=\\#]', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top }, 500, 'linear');
    });
    var window_width = $(window).width();
    if (window_width < 768) {
        $(".sticky-sidebar").trigger("sticky_kit:detach");
    } else {
        if ($('.sticky-sidebar').length) {
            make_sticky();
        }
    }

    $(window).resize(function() {
        window_width = $(window).width();
        if (window_width < 768) {
            $(".sticky-sidebar").trigger("sticky_kit:detach");
        } else {
            if ($('.sticky-sidebar').length) {
                make_sticky();
            }
        }
    });

    function make_sticky() {
        $('.sticky-sidebar').stick_in_parent({
            'parent': $('.js-sticky-container'),
            'offset_top': 50
        });
    }
    $('.search-posts.lists.menu-holder ul li a').on('click', function(e) {
        if (window_width > 768) {
            e.preventDefault();
        }
        $('.search-posts.lists.menu-holder ul li a').removeClass('is-active');
        $(this).addClass('is-active');
        var url = $(this).attr('href');
        $('.tab-contents').removeClass('is-active');
        $(url).addClass('is-active');
    });
    var j = 2;
    var i;
    var x;
    var k;
    var n;
    var m;
    var l;
    $('.blog-posts.wrap .tab-contents .readmore-btn').on('click', function(e) {
        e.preventDefault();
        const categoryId = $(this).data('category-id');
        $('.tab-contents.is-active .readmore-btn').css('pointer-events', 'none');
        if (categoryId) {
            $.get({
                    url: '/api/v1/blog/' + categoryId + '.json?page=' + j
                })
                .done(function(data) {
                    $('.tab-contents.is-active .readmore-btn').removeAttr('style');
                    for (i in data) {
                        var item = data[i];
                        var pagi = data[i].pagination;

                        for (k in pagi) {
                            var page_count = pagi.total_pages;
                            if (j > page_count) {
                                $('.tab-contents.is-active .readmore').hide();
                            }
                        }
                        for (x in item) {
                            if (typeof item[x].title !== "undefined") {
                                $('.is-active .flex-box').last().append("<div class='flex-box'><div class='row vlarge'> <div class='columns medium-3 small-12 animated fadeIn go'><div class='blog-posts posts-holder'><div class='blog-posts posts-holder image-holder'><a href=" + item[x].url + " data-id=" + item[x].id + "><img src=" + item[x].image + " alt=''></a></div></div></div><div class='columns medium-9 small-12 animated fadeIn go'><div class='blog-posts posts-holder'><div class='blog-posts posts-holder content-holder'><h3><a href=" + item[x].url + ">" + item[x].title + "</a></h3><div class='posts-date'>" + item[x].postDate + "</div><div class='posts-content'>" + item[x].body + "</div></div></div></div></div></div>");
                            }
                        }
                    }
                });
        }
        j++;
    });
    var n = 2;
    $('.search-posts.wrap .tab-contents .readmore-btn').on('click', function(e) {
        e.preventDefault();
        const categoryId = $(this).data('category-id');
        $('.tab-contents.is-active .readmore-btn').css('pointer-events', 'none');
        if (categoryId) {
            $.get({
                    url: '/api/v1/videos/' + categoryId + '.json?page=' + n
                })
                .done(function(data) {
                    $('.tab-contents.is-active .readmore-btn').removeAttr('style');
                    for (i in data) {
                        var item = data[i];
                        var pagi = data[i].pagination;

                        for (k in pagi) {
                            var page_count = pagi.total_pages;
                            if (n > page_count) {
                                $('.tab-contents.is-active .readmore').hide();
                            }
                        }
                        for (x in item) {
                            if (typeof item[x].title !== "undefined") {
                                if(item[x].isNew == 'Yes'){
                                    $('.is-active.video .video-posts').last().after('<div class="columns large-4 medium-6 small-12 animated fadeIn video-posts go"><div class="search-posts posts-holder ajaxVideo new"><div class="search-posts posts-holder image-holder"><a href="/video_lists?id='+item[x].id+'" data-id="'+item[x].id+'" data-rel="lightcase:videoCollection"><img src="'+item[x].image+'" alt=""></a><div class="caption">New</div></div><div class="search-posts posts-holder content-holder"><h3><a href="'+item[x].url+'">'+item[x].title+'</a></h3><div class="search-date">'+item[x].postDate+'</div></div></div></div>');
                                }else{
                                    $('.is-active.video .video-posts').last().after('<div class="columns large-4 medium-6 small-12 animated fadeIn video-posts go"><div class="search-posts posts-holder ajaxVideo new"><div class="search-posts posts-holder image-holder"><a href="/video_lists?id='+item[x].id+'" data-id="'+item[x].id+'" data-rel="lightcase:videoCollection"><img src="'+item[x].image+'" alt=""></a><div class="caption"></div></div><div class="search-posts posts-holder content-holder"><h3><a href="'+item[x].url+'">'+item[x].title+'</a></h3><div class="search-date">'+item[x].postDate+'</div></div></div></div>');
                                }
                            }
                        }
                    }
                });
        }
        n++;
    });
    var l = 2;
    $('.blog-posts.wrap #most-recent .readmore-btn').on('click', function(e) {
        e.preventDefault();
        $('.tab-contents.is-active .readmore-btn').css('pointer-events', 'none');
        $.get({
                url: '/api/v1/blog/?page=' + l
            })
            .done(function(data) {
                $('.tab-contents.is-active .readmore-btn').removeAttr('style');
                for (i in data) {
                    var item = data[i];
                    var pagi = data[i].pagination;
                    for (k in pagi) {
                        var page_count = pagi.total_pages;
                        if (l > page_count) {
                            $('.tab-contents.is-active .readmore').hide();
                        }
                    }
                    for (x in item) {
                        if (typeof item[x].title !== "undefined") {
                            $('.is-active .flex-box').last().append("<div class='flex-box'><div class='row vlarge'> <div class='columns medium-3 small-12 animated fadeIn go'><div class='blog-posts posts-holder'><div class='blog-posts posts-holder image-holder'><a href=" + item[x].url + " data-id=" + item[x].id + "><img src=" + item[x].image + " alt=''></a></div></div></div><div class='columns medium-9 small-12 animated fadeIn go'><div class='blog-posts posts-holder'><div class='blog-posts posts-holder content-holder'><h3><a href=" + item[x].url + ">" + item[x].title + "</a></h3><div class='posts-date'>" + item[x].postDate + "</div><div class='posts-content'>" + item[x].body + "</div></div></div></div></div></div>");
                        }
                    }
                }
            });
        l++;
    });
    var m = 2;
    $('.video#most-recent .readmore-btn').on('click', function(e) {
        e.preventDefault();
        $('.tab-contents.is-active .readmore-btn').css('pointer-events', 'none');
        $.get({
                url: '/api/v1/videos.json/?page=' + m
            })
            .done(function(data) {
                $('.tab-contents.is-active .readmore-btn').removeAttr('style');
                for (i in data) {
                    var item = data[i];
                    var pagi = data[i].pagination;
                    for (k in pagi) {
                        var page_count = pagi.total_pages;
                        if (m > page_count) {
                            $('.tab-contents.is-active .readmore').hide();
                        }
                    }
                    for (x in item) {
                        if (typeof item[x].title !== "undefined") {
                            if(item[x].isNew == 'Yes'){
                                $('.is-active.video .video-posts').last().after('<div class="columns large-4 medium-6 small-12 animated fadeIn video-posts go"><div class="search-posts posts-holder ajaxVideo new"><div class="search-posts posts-holder image-holder"><a href="/video_lists?id=199" data-id="199" data-rel="lightcase:videoCollection"><img src="'+item[x].image+'" alt=""></a><div class="caption">New</div></div><div class="search-posts posts-holder content-holder"><h3><a href="'+item[x].url+'">'+item[x].title+'</a></h3><div class="search-date">'+item[x].postDate+'</div></div></div></div>');
                            }else{
                                $('.is-active.video .video-posts').last().after('<div class="columns large-4 medium-6 small-12 animated fadeIn video-posts go"><div class="search-posts posts-holder ajaxVideo new"><div class="search-posts posts-holder image-holder"><a href="/video_lists?id=199" data-id="199" data-rel="lightcase:videoCollection"><img src="'+item[x].image+'" alt=""></a><div class="caption"></div></div><div class="search-posts posts-holder content-holder"><h3><a href="'+item[x].url+'">'+item[x].title+'</a></h3><div class="search-date">'+item[x].postDate+'</div></div></div></div>');
                            }
                        }
                    }
                }
            });
        m++;
    });
})();
