var x = 0;
$('divSelector .readmore a').on('click', function(e) {
e.preventDefault();
	$.ajax({
		url: '/ajax/posts',
		data: {
		    minRow: 1,
		    maxRow: 3,
		    limit: 2, 
		    order: 'desc'
		},
		dataType: 'json',
		success: function(data){
		    if($('divSelector .blog-posts.posts-holder.image-holder a[data-id='+data[x].siteID+']').length == 0) { 
		        $('divSelector .flex-box').last().after("<div class='flex-box'><div class='row vlarge'> <div class='columns medium-3 small-12 animated fadeIn'><div class='blog-posts posts-holder'><div class='blog-posts posts-holder image-holder'><a href='+data[x].siteUrl + data[x].slug +' data-id='+data[x].siteID+'><img src='+data[x].featuredImage+' alt=''></a></div></div></div><div class='columns medium-9 small-12 animated fadeIn go'><div class='blog-posts posts-holder'><div class='blog-posts posts-holder content-holder'><h3><a href='+data[x].siteUrl + data[x].slug +'>'+data[x].title+'</a></h3><div class='posts-date'>'+data[x].postDate+'</div><div class='posts-content'>'+data[x].content+'</div></div></div></div></div></div>");
		    }
		}    
	});  
x++;   
});