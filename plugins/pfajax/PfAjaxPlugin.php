<?php
namespace Craft;

class PfAjaxPlugin extends BasePlugin{
     function getName()
     {
         return Craft::t('PF Ajax');
     }

    function getVersion()
    {
        return '1.0';
    }

    function getDeveloper()
    {
        return 'Patrick Flores';
    }

    function getDeveloperUrl()
    {
        return 'http://www.patrickianflores.com';
    }
    public function addTwigExtension()
    {
        Craft::import('plugins.pfajax.postextension.PFAjaxPostExtension');
        return new PFAjaxPostExtension();
    }
}