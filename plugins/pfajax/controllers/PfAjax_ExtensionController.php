<?php
namespace Craft;

class PfAjax_ExtensionController extends BaseController{
    public function actionGetEntry(){
    	$this->requireAjaxRequest();
	    $response = [
	        'status' => 'open'
	    ];
	    $this->returnJson($response);
    }
}